/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Ondřej Staněk
 */
public class IngredientTableModel extends AbstractTableModel {
    
	private static final Logger logger = Logger.getLogger(
            IngredientManagerImpl.class.getName());
	
    private List<Ingredient> ingredients = new ArrayList();
    private IngredientManagerImpl ingrManager;
    private ResourceBundle bundler;

    public void setBundler(ResourceBundle bundler) {
        this.bundler = bundler;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void setIngrManager(IngredientManagerImpl ingrManager) {
        this.ingrManager = ingrManager;
    }

    @Override
    public int getRowCount() {
        return ingredients.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return bundler.getString("IngredientsTable.column0");
            case 1:
                return bundler.getString("IngredientsTable.column1");
            default:
				logger.log(Level.SEVERE, "Wrong column index");
                throw new IllegalArgumentException("columnIndex");
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Ingredient ingr = ingredients.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return ingr.getId();
            case 1:
                return ingr.getName();
            default:
				logger.log(Level.SEVERE, "Wrong column index");
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return String.class;
            default:
				logger.log(Level.SEVERE, "Wrong column index");
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Ingredient ingredient = ingredients.get(rowIndex);
        switch (columnIndex) {
            case 0:
                break;
            case 1:
                ingredient.setName((String) aValue);
                break;
            default:
				logger.log(Level.SEVERE, "Wrong column index");
                throw new IllegalArgumentException("columnIndex");
        }
        ingrManager.updateIngredient(ingredient);
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return false;
            case 1:
                return true;
            default:
				logger.log(Level.SEVERE, "Wrong column index");
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
}
