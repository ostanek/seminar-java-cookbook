/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author Ondřej Staněk
 */
public class Cookbook {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                JFrame window = new Gui();
                window.pack();
                window.setVisible(true);
            }
        });
    }
    
}
