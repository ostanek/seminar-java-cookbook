/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import java.util.List;

/**
 *
 * @author Ondřej Staněk
 */
public interface RecipeManager {
    public void addRecipe(Recipe recipe);
    public void deleteRecipe(Recipe recipe);
    public void updateRecipe(Recipe newRecipe);
    public List<Recipe> getRecipes();
    //public List<Recipe> findByIngredient(Ingredient ingredient);
    public Recipe findById(int id);
}
