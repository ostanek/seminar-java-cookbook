/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import java.util.List;

/**
 *
 * @author boris
 */
public interface NeedManager {
	public void addNeed(Need need);
    public void deleteNeed(Need need);
    public void updateNeed(Need need);
	public List<Ingredient> findIngredientsByRecipe(Recipe recipe);
	public List<Recipe> findRecipesByIngredient(Ingredient ingr);
        public List<Need> findNeedsByRecipe(Recipe recipe);
}
