/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

/**
 *
 * @author boris
 */
public class Need {
	private int recipeID;
	private int ingredientID;
	private float amount;
	private String unit;

	public Need() {
	}

	public Need(int recipeID, int ingredientID, float amount, String unit) {
		this.recipeID = recipeID;
		this.ingredientID = ingredientID;
		this.amount = amount;
		this.unit = unit;
	}

	public int getRecipeID() {
		return recipeID;
	}

	public void setRecipeID(int recipeID) {
		this.recipeID = recipeID;
	}

	public int getIngredientID() {
		return ingredientID;
	}

	public void setIngredientID(int ingredientID) {
		this.ingredientID = ingredientID;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 89 * hash + this.recipeID;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Need other = (Need) obj;
		if (this.recipeID != other.recipeID) {
			return false;
		}
		return true;
	}
	
}
