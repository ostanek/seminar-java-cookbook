/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import common.DBUtils;
import common.ServiceFailureException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author boris
 */
public class NeedManagerImpl implements NeedManager{

	private static final Logger logger = Logger.getLogger(
            RecipeManagerImpl.class.getName());

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }    

    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }
	
	@Override
	public void addNeed(Need need) {
		checkDataSource();
        
        if (need == null)
            throw new IllegalArgumentException("Need is null.");
        
        Connection conn = null;
        PreparedStatement st = null;
		
		try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("INSERT INTO NEED (RECIPEID, INGREDIENTID, AMOUNT, UNIT) VALUES (?,?,?,?)");
            st.setInt(1,need.getRecipeID());
			st.setInt(2,need.getIngredientID());
			st.setFloat(3,need.getAmount());
			st.setString(4,need.getUnit());
            st.execute();
            
        } catch (SQLException ex) {
            String msg = "Error when trying to add need " + need;
            Logger.getLogger(RecipeManagerImpl.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
	}

	@Override
	public void deleteNeed(Need need) {
		checkDataSource();
        
        if (need == null)
                throw new NullPointerException();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("DELETE FROM NEED WHERE RECIPEID = ? AND INGREDIENTID = ?");
            st.setInt(1, need.getRecipeID());
			st.setInt(2, need.getIngredientID());
            st.execute();
            
        } catch (SQLException ex) {
            String msg = "Error when trying to delete need " + need;
            Logger.getLogger(RecipeManagerImpl.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
	}

	@Override
	public void updateNeed(Need need) {
		checkDataSource();
        
        if (need == null)
            throw new NullPointerException();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("UPDATE NEED SET AMOUNT = ?, UNIT = ? WHERE RECIPEID = ? AND INGREDIENTID = ?");
            st.setFloat(1, need.getAmount());
			st.setString(2, need.getUnit());
            st.setInt(3, need.getRecipeID());
			st.setInt(4, need.getIngredientID());
            st.execute();
            
        } catch (SQLException ex) {
            String msg = "Error when trying to update need " + need;
            Logger.getLogger(RecipeManagerImpl.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
	}

	@Override
	public List<Ingredient> findIngredientsByRecipe(Recipe recipe) {
		checkDataSource();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        Ingredient ingr = null;
        List<Ingredient> lIngredients = new ArrayList<>();
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT INGREDIENTS.ID, INGREDIENTS.NAME FROM INGREDIENTS JOIN NEED " +
                    "ON NEED.INGREDIENTID = INGREDIENTS.ID " +
                    "WHERE RECIPEID = ?");
            st.setInt(1, recipe.getId());
            ResultSet returnedIngredients = st.executeQuery();
            
            while(returnedIngredients.next()) {
                ingr = new Ingredient();
                ingr.setId(returnedIngredients.getInt("ID"));
                ingr.setName(returnedIngredients.getString("NAME"));
                lIngredients.add(ingr);
            }
			
        } catch (SQLException ex) {
            String msg = "Error when trying to find recipe with ingredient named "/* + ingr.getName()*/;
            Logger.getLogger(RecipeManagerImpl.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
        
        return lIngredients;
	}

	@Override
	public List<Recipe> findRecipesByIngredient(Ingredient ingr) {
		checkDataSource();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        Recipe recipe = null;
        List<Recipe> lRecipes = new ArrayList<>();
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT RECIPES.* FROM RECIPES JOIN NEED " +
                    "ON NEED.INGREDIENTID = RECIPES.ID " +
                    "WHERE RECIPES.ID = ?");
            st.setInt(1, ingr.getId());
            ResultSet returnedRecipes = st.executeQuery();
            
            while(returnedRecipes.next()) {
                recipe = new Recipe();
                recipe.setId(returnedRecipes.getInt("ID"));
                recipe.setName(returnedRecipes.getString("NAME"));
                recipe.setCreateDate(returnedRecipes.getString("CREATEDATE"));
                recipe.setAuthor(returnedRecipes.getString("CREATEPERSON"));
                lRecipes.add(recipe);
            }
            
        } catch (SQLException ex) {
            String msg = "Error when trying to find recipe with ingredient named " + ingr.getName();
            Logger.getLogger(RecipeManagerImpl.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
        
        return lRecipes;
	}
        
        @Override
	public List<Need> findNeedsByRecipe(Recipe recipe) {
		checkDataSource();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        Need need = null;
        List<Need> lNeeds = new ArrayList<>();
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT * FROM NEED " +
                    "WHERE RECIPEID = ?");
            st.setInt(1, recipe.getId());
            ResultSet returnedNeeds = st.executeQuery();
            
            while(returnedNeeds.next()) {
                need = new Need();
                need.setRecipeID(returnedNeeds.getInt("RECIPEID"));
                need.setIngredientID(returnedNeeds.getInt("INGREDIENTID"));
                need.setAmount(returnedNeeds.getFloat("AMOUNT"));
                need.setUnit(returnedNeeds.getString("UNIT"));
                lNeeds.add(need);
            }
			
        } catch (SQLException ex) {
            String msg = "Error when trying to find need with recipe named " + recipe.getName();
            Logger.getLogger(NeedManager.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
        
        return lNeeds;
	}
	
}
