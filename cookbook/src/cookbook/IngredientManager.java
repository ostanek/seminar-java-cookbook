/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import java.util.List;

/**
 *
 * @author boris
 */
public interface IngredientManager {
	public void addIngredient(Ingredient ingr);
    public void deleteIngredient(Ingredient ingr);
    public void updateIngredient(Ingredient ingr);
    public List<Ingredient> getIngredients();
    public Ingredient findById(int id);
}
