/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Ondřej Staněk
 */
public class RecipesTableModel extends AbstractTableModel {
    
	private static final Logger logger = Logger.getLogger(
            IngredientManagerImpl.class.getName());
	
    private List<Recipe> recipes = new ArrayList<>();
    private RecipeManagerImpl recipeManager;
    private ResourceBundle bundler;
    
    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipeManager(RecipeManagerImpl recipeManager) {
        this.recipeManager = recipeManager;
    }

    public void setBundler(ResourceBundle bundler){
	this.bundler = bundler;
    }
	
    @Override
    public int getRowCount() {
        return recipes.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Recipe recipe = recipes.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return recipe.getId();
            case 1:
                return recipe.getName();
            case 2:
                return recipe.getCreateDate();
            case 3:
                return recipe.getAuthor();
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    public void addRecipe(Recipe recipe) {
        recipes.add(recipe);
    }
    
    public void setRecipes(List recipes) {
        this.recipes = recipes;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return bundler.getString("RecipesTable.column0");
            case 1:
                return bundler.getString("RecipesTable.column1");
            case 2:
                return bundler.getString("RecipesTable.column2");
            case 3:
                return bundler.getString("RecipesTable.column3");
            default:
				logger.log(Level.SEVERE, "Wrong column index");
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return String.class;            
            default:
				logger.log(Level.SEVERE, "Wrong column index");
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Recipe recipe = recipes.get(rowIndex);
        switch (columnIndex) {
            case 0:
                recipe.setId((Integer) aValue);
                break;
            case 1:
                recipe.setName((String) aValue);
                break;
            case 2:
                recipe.setCreateDate((String) aValue);
                break;
            case 3:
                recipe.setAuthor((String) aValue);
                break;
            default:
				logger.log(Level.SEVERE, "Wrong column index");
                throw new IllegalArgumentException("columnIndex");
        }
        recipeManager.updateRecipe(recipe);
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return false;
            case 1:
            case 2:
            case 3:
                return true;
            default:
				logger.log(Level.SEVERE, "Wrong column index");
                throw new IllegalArgumentException("columnIndex");
        }
    }
}
