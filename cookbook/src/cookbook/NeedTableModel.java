/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Ondřej Staněk
 */
public class NeedTableModel extends AbstractTableModel {
    
    private List<Need> needs = new ArrayList<>();
    private NeedManagerImpl needManager;
    private IngredientManagerImpl ingredientManager;
    private ResourceBundle bundler;
    
    public void setBundler(ResourceBundle bundler){
	this.bundler = bundler;
    }

    public List<Need> getNeeds() {
        return needs;
    }

    public void setIngredientManager(IngredientManagerImpl ingredientManager) {
        this.ingredientManager = ingredientManager;
    }

    public void setNeedManager(NeedManagerImpl needManager) {
        this.needManager = needManager;
    }

    @Override
    public int getRowCount() {
        return needs.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Need need = needs.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return ingredientManager.findById(need.getIngredientID()).getName();
            case 1:
                return need.getAmount();
            case 2:
                return need.getUnit();
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    public void setNeeds(List needs) {
        this.needs = needs;
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return bundler.getString("NeedsTable.column0");
            case 1:
                return bundler.getString("NeedsTable.column1");
            case 2:
                return bundler.getString("NeedsTable.column2");
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return Float.class;
            case 2:
                return String.class;
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Need need = needs.get(rowIndex);
        switch (columnIndex) {
            case 0:
                break;
            case 1:
                need.setAmount((Float) aValue);
                break;
            case 2:
                need.setUnit((String) aValue);
                break;
            default:
                throw new IllegalArgumentException("columnIndex");
        }
        needManager.updateNeed(need);
        fireTableCellUpdated(rowIndex, columnIndex);
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return false;
            case 1:
            case 2:
                return true;
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
}
