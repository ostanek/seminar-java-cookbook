/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import common.DBUtils;
import common.ServiceFailureException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author boris
 */
public class IngredientManagerImpl implements IngredientManager{

	private static final Logger logger = Logger.getLogger(
            IngredientManagerImpl.class.getName());

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }    

    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }
	
	@Override
	public void addIngredient(Ingredient ingr) throws ServiceFailureException{
		checkDataSource();
        
        if (ingr == null)
            throw new IllegalArgumentException("Ingredient is null.");
        
        Connection conn = null;
        PreparedStatement st = null;
		
		try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("INSERT INTO INGREDIENTS (NAME) VALUES (?)");
            st.setString(1,ingr.getName());
            st.execute();
            
        } catch (SQLException ex) {
            String msg = "Error when trying to add ingredient " + ingr;
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
	}

	@Override
	public void deleteIngredient(Ingredient ingr) {
		checkDataSource();
        
        if (ingr == null)
                throw new NullPointerException();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("DELETE FROM INGREDIENTS WHERE ID = ?");
            st.setInt(1, ingr.getId());
            st.execute();
            
        } catch (SQLException ex) {
            String msg = "Error when trying to delete ingredient " + ingr;
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
	}

	@Override
	public void updateIngredient(Ingredient ingr) {
		checkDataSource();
        
        if (ingr == null)
            throw new NullPointerException();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("UPDATE INGREDIENTS SET NAME = ? WHERE ID = ?");
            st.setString(1, ingr.getName());
            st.setInt(2, ingr.getId());
            st.execute();
            
        } catch (SQLException ex) {
            String msg = "Error when trying to update ingredient " + ingr;
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
	}

	@Override
	public List<Ingredient> getIngredients() {
		checkDataSource();
        
        List<Ingredient> ingredients = new ArrayList<>();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT * FROM INGREDIENTS");
            ResultSet returnedRecipes = st.executeQuery();
            
            while(returnedRecipes.next()) {
                Ingredient ingr = new Ingredient();
                ingr.setId(returnedRecipes.getInt("ID"));
                ingr.setName(returnedRecipes.getString("NAME"));
                ingredients.add(ingr);
            }
            
        } catch (SQLException ex) {
            String msg = "Error when trying to get all ingrdients.";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
        
        return ingredients;
	}

	@Override
	public Ingredient findById(int id) {
		checkDataSource();
        
        Ingredient ingr = null;
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT * FROM INGREDIENTS WHERE ID = ?");
            st.setInt(1, id);
            ResultSet returnedRecipes = st.executeQuery();
            
            while(returnedRecipes.next()) {
                ingr = new Ingredient();
                ingr.setId(returnedRecipes.getInt("ID"));
                ingr.setName(returnedRecipes.getString("NAME"));
            }
            
        } catch (SQLException ex) {
            String msg = "Error when trying to find ingredient with id " + id;
            Logger.getLogger(IngredientManagerImpl.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
        
        return ingr;
	}
	
}
