/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import javax.swing.SwingWorker;
import org.apache.commons.dbcp.BasicDataSource;
//import org.apache.derby.iapi.services.i18n.BundleFinder;

/**
 *
 * @author Ondřej Staněk
 */
public class Gui extends javax.swing.JFrame {
	
	public static final Logger logger = Logger.getLogger(
            IngredientManagerImpl.class.getName());
	
    private DataSource ds;
    private RecipeManagerImpl recipeManager;
    private NeedManagerImpl needManager;
    private IngredientManagerImpl ingrManager;
    private NeedTableModel needsModel;
    private RecipesTableModel recipesModel;
    private String selectedRecipeName = "";
    private DbSwingWorker dbSwingWorker;
    private ResourceBundle bundler;
    private Properties prop = null;
 
    private class DbSwingWorker extends SwingWorker<DataSource,Void> {
		
        @Override
        protected DataSource doInBackground() throws Exception {
            DataSource result = prepareDataSource(prop);
            return result;
        }
        
        @Override
        protected void done() {
            try {
                ds = get();
                if (ds == null) {
                    statusBar.setText("Nepodarilo se pripojit k databazi. Spustte prosim databazi.");
					logger.log(Level.SEVERE, "Error while databse connection");
                    return;
                }
                
            } catch (ExecutionException ex) {
                statusBar.setText("Nepodarilo se pripojit k databazi. Spustte prosim databazi.");
				logger.log(Level.SEVERE, "Error while databse connection", ex);
            } catch (InterruptedException ex) {
                // K tomuto by v tomto případě nemělo nikdy dojít (viz níže)
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                throw new RuntimeException("Operace prerusena (toto by se nemelo nikdy stat).",ex);
            }
            
            Connection conn = null;
            PreparedStatement st = null;
            try {
                conn = ds.getConnection();
            } catch (SQLException ex) {
                statusBar.setText("Nepodarilo se pripojit k databazi. Spustte prosim databazi.");
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                return;
            }
            finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                    }
                }
            }
		
            statusBar.setText("Aplikace se uspesne pripojila k databazi.");
			logger.log(Level.INFO, "Databse connection established");
            
            recipeManager = new RecipeManagerImpl();
            recipeManager.setDataSource(ds);

            ingrManager = new IngredientManagerImpl();
            ingrManager.setDataSource(ds);

            recipesModel.setRecipeManager(recipeManager);
            
            needManager = new NeedManagerImpl();
            needManager.setDataSource(ds);
        
            needsModel.setNeedManager(needManager);
            needsModel.setIngredientManager(ingrManager);
            
            GetRecipesSwingWorker getRecipesWorker = new GetRecipesSwingWorker();
            getRecipesWorker.execute();
            GetFirstRecipeNeedsSwingWorker firstNeedWorker = new GetFirstRecipeNeedsSwingWorker();
            firstNeedWorker.execute();
        }
    }
    
    private class ConfigWorker extends SwingWorker<Boolean,Void> {
        @Override
        protected Boolean doInBackground() throws Exception {
            prop = new Properties();
            InputStream is = this.getClass().getResourceAsStream("/cookbook/config.properties");
            prop.load(is);
            return true;
        }
        
        @Override
        protected void done() {
            try {
                get();
                dbSwingWorker = new DbSwingWorker();
                dbSwingWorker.execute();
            } catch (ExecutionException ex) {
                statusBar.setText("Chyba v nastaveni pripojeni k databazi. Opravte soubor config.properties. " + ex);
				logger.log(Level.SEVERE, "Error in config file setting");
            } catch (InterruptedException ex) {
                // K tomuto by v tomto případě nemělo nikdy dojít (viz níže)
				logger.log(Level.SEVERE, "Error in databse connection while config file loading", ex);
                throw new RuntimeException("Operace prerusena (toto by se nemelo nikdy stat).",ex);
				
            }
        }
    }

    /**
     * Creates new form Gui
     */
    public Gui() {
        String userLang = System.getProperty("user.language");
        String userCountry = System.getProperty("user.country");
        bundler = ResourceBundle.getBundle("cookbook.Bundle", new Locale(userLang, userCountry));
        
        recipesModel = new RecipesTableModel();
        recipesModel.setBundler(bundler);
        
        needsModel = new NeedTableModel();
        needsModel.setBundler(bundler);
        
        initComponents();
		
        ConfigWorker configWorker = new ConfigWorker();
        configWorker.execute();
    }
    
    public DataSource prepareDataSource(Properties prop) throws SQLException {	
        BasicDataSource ds = new BasicDataSource();
	ds.setUrl(prop.getProperty("dbPath"));//"jdbc:derby://localhost:1527/ROOT"); 
        ds.setUsername(prop.getProperty("user"));//"root");
        ds.setPassword(prop.getProperty("pass"));//"root");
        return ds;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        editRecipeDialog = new javax.swing.JDialog();
        statusBar = new javax.swing.JTextField();
        deleteRecipeButton = new javax.swing.JButton();
        showNeedsButton = new javax.swing.JButton();
        listRecipesLabel = new javax.swing.JLabel();
        listNeedsLabel = new javax.swing.JLabel();
        deleteNeedButton = new javax.swing.JButton();
        addNeed = new javax.swing.JButton();
        tableRecipesScrollPane = new javax.swing.JScrollPane();
        tableRecipes = new javax.swing.JTable();
        tableNeedsScrollPane = new javax.swing.JScrollPane();
        tableNeeds = new javax.swing.JTable();
        menuBar = new javax.swing.JMenuBar();
        mainMenu = new javax.swing.JMenu();
        exit = new javax.swing.JMenuItem();
        recipesMenu = new javax.swing.JMenu();
        addRecipe = new javax.swing.JMenuItem();
        ingredients = new javax.swing.JMenu();
        showIngredients = new javax.swing.JMenuItem();
        addIngredient = new javax.swing.JMenuItem();

        javax.swing.GroupLayout editRecipeDialogLayout = new javax.swing.GroupLayout(editRecipeDialog.getContentPane());
        editRecipeDialog.getContentPane().setLayout(editRecipeDialogLayout);
        editRecipeDialogLayout.setHorizontalGroup(
            editRecipeDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 472, Short.MAX_VALUE)
        );
        editRecipeDialogLayout.setVerticalGroup(
            editRecipeDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 333, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        statusBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statusBarActionPerformed(evt);
            }
        });

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("cookbook/Bundle"); // NOI18N
        deleteRecipeButton.setText(bundle.getString("Gui.deleteRecipeButton.text")); // NOI18N
        deleteRecipeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteRecipeButtonActionPerformed(evt);
            }
        });

        showNeedsButton.setText(bundle.getString("Gui.showNeedsButton.text")); // NOI18N
        showNeedsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showNeedsButtonActionPerformed(evt);
            }
        });

        listRecipesLabel.setText(bundle.getString("Gui.listRecipesLabel.text")); // NOI18N

        listNeedsLabel.setText(bundle.getString("Gui.listNeedsLabel.text")); // NOI18N

        deleteNeedButton.setText(bundle.getString("Gui.deleteNeedButton.text")); // NOI18N
        deleteNeedButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteNeedButtonActionPerformed(evt);
            }
        });

        addNeed.setText(bundle.getString("Gui.addNeed.text")); // NOI18N
        addNeed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addNeedActionPerformed(evt);
            }
        });

        tableRecipes.setModel(recipesModel);
        tableRecipesScrollPane.setViewportView(tableRecipes);

        tableNeeds.setModel(needsModel);
        tableNeedsScrollPane.setViewportView(tableNeeds);

        mainMenu.setText(bundle.getString("Gui.mainMenu.text")); // NOI18N

        exit.setText(bundle.getString("Gui.exit.text")); // NOI18N
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });
        mainMenu.add(exit);

        menuBar.add(mainMenu);

        recipesMenu.setText(bundle.getString("Gui.recipesMenu.text")); // NOI18N

        addRecipe.setText(bundle.getString("Gui.addRecipe.text")); // NOI18N
        addRecipe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addRecipeActionPerformed(evt);
            }
        });
        recipesMenu.add(addRecipe);

        menuBar.add(recipesMenu);

        ingredients.setText(bundle.getString("Gui.ingredients.text")); // NOI18N

        showIngredients.setText(bundle.getString("Gui.showIngredients.text")); // NOI18N
        showIngredients.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showIngredientsActionPerformed(evt);
            }
        });
        ingredients.add(showIngredients);

        addIngredient.setText(bundle.getString("Gui.addIngredient.text")); // NOI18N
        addIngredient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addIngredientActionPerformed(evt);
            }
        });
        ingredients.add(addIngredient);

        menuBar.add(ingredients);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusBar)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(listRecipesLabel)
                    .addComponent(deleteRecipeButton)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(tableRecipesScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(showNeedsButton)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(listNeedsLabel)
                    .addComponent(tableNeedsScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addNeed)
                    .addComponent(deleteNeedButton)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(listRecipesLabel)
                    .addComponent(listNeedsLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tableRecipesScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 322, Short.MAX_VALUE)
                    .addComponent(tableNeedsScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(showNeedsButton)
                    .addComponent(addNeed))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(deleteRecipeButton)
                    .addComponent(deleteNeedButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(statusBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        listNeedsLabel.setText("Suroviny vybraného receptu: " + selectedRecipeName);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    private void statusBarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statusBarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_statusBarActionPerformed
    
    private class GetFirstRecipeNeedsSwingWorker extends SwingWorker<Recipe,Void> {
        
        @Override
        protected Recipe doInBackground() throws Exception {
            Recipe firstRecipe = recipeManager.findById(1);
            return firstRecipe;
        }
        
         @Override
        protected void done() {
            try {
                Recipe firstRecipe = get();
                if (firstRecipe != null) {
                    GetNeedsSwingWorker lNeedsWorker = new GetNeedsSwingWorker();
                    lNeedsWorker.setRecipe(firstRecipe);
                    lNeedsWorker.execute();
                    selectedRecipeName = firstRecipe.getName();
                    listNeedsLabel.setText("Suroviny vybraného receptu: " + selectedRecipeName);
                }
                else {
                    needsModel.setNeeds(new ArrayList<>());
                    needsModel.fireTableDataChanged();
                }
            } catch (ExecutionException ex) {
                statusBar.setText("Nepodarilo se pripojit k databazi. Spustte prosim databazi.");
				logger.log(Level.SEVERE, "Error while databse connection");
            } catch (InterruptedException ex) {
                // K tomuto by v tomto případě nemělo nikdy dojít (viz níže)
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                throw new RuntimeException("Operace prerusena (toto by se nemelo nikdy stat).",ex);
            }
        }
    }
    
    private class GetNeedsSwingWorker extends SwingWorker<List<Need>,Void> {
        private Recipe mRecipe;

        public void setRecipe(Recipe aRecipe) {
            this.mRecipe = aRecipe;
        }
        
        @Override
        protected List<Need> doInBackground() throws Exception {
            List<Need> needs = needManager.findNeedsByRecipe(mRecipe);
            return needs;
        }
        
         @Override
        protected void done() {
            try {
                needsModel.setNeeds(get());
                needsModel.fireTableDataChanged();
            } catch (ExecutionException ex) {
                statusBar.setText("Nepodarilo se pripojit k databazi. Spustte prosim databazi.");
				logger.log(Level.SEVERE, "Error while databse connection", ex);
            } catch (InterruptedException ex) {
                // K tomuto by v tomto případě nemělo nikdy dojít (viz níže)
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                throw new RuntimeException("Operace prerusena (toto by se nemelo nikdy stat).",ex);
            }
        }
    }
    
    
    private void addRecipeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addRecipeActionPerformed
        GuiAddRecipe guiAddRecipe = new GuiAddRecipe(this, true);
        guiAddRecipe.setRecipeManager(recipeManager);
        guiAddRecipe.setVisible(true);
        guiAddRecipe.pack();
        GetRecipesSwingWorker getRecipesWorker = new GetRecipesSwingWorker();
        getRecipesWorker.execute();
    }//GEN-LAST:event_addRecipeActionPerformed

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitActionPerformed

    private class ShowIngredientsSwingWorker extends SwingWorker<List<Ingredient>,Void> {
        private GuiIngredients guiIngredients;        

        public void setGuiIngredients(GuiIngredients guiIngredients) {
            this.guiIngredients = guiIngredients;
        }
        
        @Override
        protected List<Ingredient> doInBackground() throws Exception {
            List<Ingredient> lIngredients = ingrManager.getIngredients();
            return lIngredients;
        }
        
        @Override
        protected void done() {
            try {
                List<Ingredient> lIngredients = get();
                guiIngredients.initTable(lIngredients);
                guiIngredients.setVisible(true);
                guiIngredients.pack();
                needsModel.fireTableDataChanged();
            } catch (ExecutionException ex) {
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                statusBar.setText("Nepodarilo se pripojit k databazi. Spustte prosim databazi.");
            } catch (InterruptedException ex) {
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                // K tomuto by v tomto případě nemělo nikdy dojít (viz níže)
                throw new RuntimeException("Operace prerusena (toto by se nemelo nikdy stat).",ex);
            }
        }
    }
    
    private void showIngredientsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showIngredientsActionPerformed
        GuiIngredients guiIngredients = new GuiIngredients(this, true, bundler);
        guiIngredients.setIngrManager(ingrManager);
        guiIngredients.setTableIngrManager(ingrManager);
        ShowIngredientsSwingWorker show = new ShowIngredientsSwingWorker();
        show.setGuiIngredients(guiIngredients);
        show.execute();
    }//GEN-LAST:event_showIngredientsActionPerformed
    
    private void showNeedsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showNeedsButtonActionPerformed
        int selectedRecipe = tableRecipes.getSelectedRow();
        if (selectedRecipe == -1)
            return;
        List<Recipe> lRecipes = recipesModel.getRecipes();
        Recipe recipe = lRecipes.get(selectedRecipe);
        listNeedsLabel.setText("Suroviny vybraného receptu: " + recipe.getName());
        GetNeedsSwingWorker showWorker = new GetNeedsSwingWorker();
        showWorker.setRecipe(recipe);
        showWorker.execute();
    }//GEN-LAST:event_showNeedsButtonActionPerformed

    private class GetRecipesSwingWorker extends SwingWorker<List<Recipe>,Void> {
        @Override
        protected List<Recipe> doInBackground() throws Exception {
            List<Recipe> lRecipes = recipeManager.getRecipes();
            return lRecipes;
        }
        
        @Override
        protected void done() {
            try {
                List<Recipe> lRecipes = get();
                recipesModel.setRecipes(lRecipes);
                recipesModel.fireTableDataChanged();
            } catch (ExecutionException ex) {
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                statusBar.setText("Nepodarilo se pripojit k databazi. Spustte prosim databazi.");
            } catch (InterruptedException ex) {
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                // K tomuto by v tomto případě nemělo nikdy dojít (viz níže)
                throw new RuntimeException("Operace prerusena (toto by se nemelo nikdy stat).",ex);
            }
        }
    }
    
    private class DeleteRecipeSwingWorker extends SwingWorker<Boolean,Void> {
        private Recipe mRecipe;

        public void setRecipe(Recipe aRecipe) {
            this.mRecipe = aRecipe;
        }
        
        @Override
        protected Boolean doInBackground() throws Exception {
            recipeManager.deleteRecipe(mRecipe);
            return true;
        }
        
         @Override
        protected void done() {
            try {
                get();
                GetRecipesSwingWorker getWorker = new GetRecipesSwingWorker();
                getWorker.execute();
                GetFirstRecipeNeedsSwingWorker firstWorker = new GetFirstRecipeNeedsSwingWorker();
                firstWorker.execute();
            } catch (ExecutionException ex) {
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                statusBar.setText("Nepodarilo se pripojit k databazi. Spustte prosim databazi.");
            } catch (InterruptedException ex) {
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                // K tomuto by v tomto případě nemělo nikdy dojít (viz níže)
                throw new RuntimeException("Operace prerusena (toto by se nemelo nikdy stat).",ex);
            }
        }
    }
    
    private void deleteRecipeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteRecipeButtonActionPerformed
        int selectedRecipe = tableRecipes.getSelectedRow();
        List<Recipe> recipes = recipesModel.getRecipes();
        Recipe recipe = recipes.get(selectedRecipe);
        DeleteRecipeSwingWorker deleteWorker = new DeleteRecipeSwingWorker();
        deleteWorker.setRecipe(recipe);
        deleteWorker.execute();
    }//GEN-LAST:event_deleteRecipeButtonActionPerformed

    private class DeleteNeedSwingWorker extends SwingWorker<Boolean,Void> {
        private Need mNeed;
        private Recipe mRecipe;

        public void setRecipe(Recipe aRecipe) {
            this.mRecipe = aRecipe;
        }

        public void setNeed(Need aNeed) {
            this.mNeed = aNeed;
        }
        
        @Override
        protected Boolean doInBackground() throws Exception {
            needManager.deleteNeed(mNeed);
            return true;
        }
        
         @Override
        protected void done() {
            try {
                get();
                GetNeedsSwingWorker showNeedsWorker = new GetNeedsSwingWorker();
                showNeedsWorker.setRecipe(mRecipe);
                showNeedsWorker.execute();
            } catch (ExecutionException ex) {
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                statusBar.setText("Nepodarilo se pripojit k databazi. Spustte prosim databazi.");
            } catch (InterruptedException ex) {
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                // K tomuto by v tomto případě nemělo nikdy dojít (viz níže)
                throw new RuntimeException("Operace prerusena (toto by se nemelo nikdy stat).",ex);
            }
        }
    }
    
    private void deleteNeedButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteNeedButtonActionPerformed
        int selectedRecipe = tableRecipes.getSelectedRow();
        if (selectedRecipe == -1)
            return;
        List<Recipe> recipes = recipesModel.getRecipes();
        Recipe recipe = recipes.get(selectedRecipe);
        int selectedNeed = tableNeeds.getSelectedRow();
        if (selectedNeed == -1)
            return;
        List<Need> needs = needsModel.getNeeds();
        Need need = needs.get(selectedNeed);
        
        DeleteNeedSwingWorker deleteNeedWorker = new DeleteNeedSwingWorker();
        deleteNeedWorker.setNeed(need);
        deleteNeedWorker.setRecipe(recipe);
        deleteNeedWorker.execute();
    }//GEN-LAST:event_deleteNeedButtonActionPerformed

    private class InitIngredientsComboSwingWorker extends SwingWorker<List<Ingredient>,Void> {
        private GuiAddNeed guiAddNeed;

        public void setGuiAddNeed(GuiAddNeed guiAddNeed) {
            this.guiAddNeed = guiAddNeed;
        }
        
        @Override
        protected List<Ingredient> doInBackground() throws Exception {
            List<Ingredient> lIngredients = ingrManager.getIngredients();
            return lIngredients;
        }
        
        @Override
        protected void done() {
            try {
                List<Ingredient> lIngredients = get();
                guiAddNeed.setIngredients(lIngredients);
                guiAddNeed.initIngrComboBox();
                
            } catch (ExecutionException ex) {
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                statusBar.setText("Nepodarilo se pripojit k databazi. Spustte prosim databazi.");
            } catch (InterruptedException ex) {
				logger.log(Level.SEVERE, "Error while databse connection", ex);
                // K tomuto by v tomto případě nemělo nikdy dojít (viz níže)
                throw new RuntimeException("Operace prerusena (toto by se nemelo nikdy stat).",ex);
            }
        }
    }
    
    private void addNeedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addNeedActionPerformed
        int selectedRecipe = tableRecipes.getSelectedRow();
        if (selectedRecipe == -1)
            return;
        
        GuiAddNeed guiAddNeed = new GuiAddNeed(this, true);
        InitIngredientsComboSwingWorker initCombo = new InitIngredientsComboSwingWorker();
        initCombo.setGuiAddNeed(guiAddNeed);
        initCombo.execute();
        
        List<Recipe> recipes = recipesModel.getRecipes();
        Recipe recipe = recipes.get(selectedRecipe);
        guiAddNeed.setRecipeSelected(recipe);
        guiAddNeed.initRecipeName();
        guiAddNeed.setNeedManager(needManager);
        guiAddNeed.setVisible(true);
        guiAddNeed.pack();
        
        GetNeedsSwingWorker showNeedsWorker = new GetNeedsSwingWorker();
        showNeedsWorker.setRecipe(recipe);
        showNeedsWorker.execute();
    }//GEN-LAST:event_addNeedActionPerformed

    private void addIngredientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addIngredientActionPerformed
        GuiAddIngredient guiAddIngr = new GuiAddIngredient(this, true);
        guiAddIngr.setIngrManager(ingrManager);
        guiAddIngr.setVisible(true);
        guiAddIngr.pack();
    }//GEN-LAST:event_addIngredientActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Gui gui = new Gui();
                gui.setVisible(true);
                gui.pack();
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem addIngredient;
    private javax.swing.JButton addNeed;
    private javax.swing.JMenuItem addRecipe;
    private javax.swing.JButton deleteNeedButton;
    private javax.swing.JButton deleteRecipeButton;
    private javax.swing.JDialog editRecipeDialog;
    private javax.swing.JMenuItem exit;
    private javax.swing.JMenu ingredients;
    private javax.swing.JLabel listNeedsLabel;
    private javax.swing.JLabel listRecipesLabel;
    private javax.swing.JMenu mainMenu;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu recipesMenu;
    private javax.swing.JMenuItem showIngredients;
    private javax.swing.JButton showNeedsButton;
    private javax.swing.JTextField statusBar;
    private javax.swing.JTable tableNeeds;
    private javax.swing.JScrollPane tableNeedsScrollPane;
    private javax.swing.JTable tableRecipes;
    private javax.swing.JScrollPane tableRecipesScrollPane;
    // End of variables declaration//GEN-END:variables
}
