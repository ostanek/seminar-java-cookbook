/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import common.DBUtils;
import static cookbook.RecipeManagerTest.prepareDataSource;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author boris
 */
public class NeedManagerTest {
	
	private NeedManagerImpl needManager;
        private RecipeManagerImpl recipeManager;
        private IngredientManagerImpl ingrManager;
        private DataSource ds;
	
	public NeedManagerTest() {
	}
	
	public static DataSource prepareDataSource() throws SQLException {
        BasicDataSource ds = new BasicDataSource();
	ds.setUrl("jdbc:derby://localhost:1527/ROOT");
        ds.setUsername("root");
        ds.setPassword("root");
        return ds;
    }
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
    public void setUp() throws SQLException {
        ds = prepareDataSource();
        needManager = new NeedManagerImpl();
        needManager.setDataSource(ds);
        recipeManager = new RecipeManagerImpl();
        recipeManager.setDataSource(ds);
        ingrManager = new IngredientManagerImpl();
        ingrManager.setDataSource(ds);
        DBUtils.executeSqlScript(ds, RecipeManager.class.getResource("createTables.sql"));
    }
    
    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(ds, RecipeManager.class.getResource("dropTables.sql"));
    }

	/**
	 * Test of addNeed method, of class NeedManager.
	 */
	@Test
	public void testAddNeed() {
            
            System.out.println("addNeed");
            Recipe lRecipe1 = new Recipe(1, "Cukrovi", "01-01-2014", "Honza");
            Ingredient lIngr1 = new Ingredient(1, "mouka");
            Need lNeed1 = new Need(1, 1, 1, "kg");
            
            // Test, zda je při přidávání receptu s hodnotou null vyhozena výjimka.
            try {
                needManager.addNeed(null);
                fail("Mela byt vyhozena vyjimka");

            }
            catch(IllegalArgumentException npe){
            }
            recipeManager.addRecipe(lRecipe1);
            ingrManager.addIngredient(lIngr1);
            needManager.addNeed(lNeed1);
            List<Recipe> listRecipes = needManager.findRecipesByIngredient(lIngr1);
            assertEquals("Nekorektni pocet receptu.", 1, listRecipes.size());
            assertTrue("Chybi recept " + lRecipe1, listRecipes.contains(lRecipe1));
	}

	/**
	 * Test of deleteNeed method, of class NeedManager.
	 */
	@Test
	public void testDeleteNeed() {
		System.out.println("addNeed");
            Recipe lRecipe1 = new Recipe(1, "Cukrovi", "01-01-2014", "Honza");
            Ingredient lIngr1 = new Ingredient(1, "mouka");
            Need lNeed1 = new Need(1, 1, 1, "kg");
            
            // Test, zda je při přidávání receptu s hodnotou null vyhozena výjimka.
            try {
                needManager.addNeed(null);
                fail("Mela byt vyhozena vyjimka");

            }
            catch(IllegalArgumentException npe){
            }
            recipeManager.addRecipe(lRecipe1);
            ingrManager.addIngredient(lIngr1);
            needManager.addNeed(lNeed1);
            List<Recipe> listRecipes = needManager.findRecipesByIngredient(lIngr1);
            assertEquals("Nekorektni pocet receptu.", 1, listRecipes.size());
            
            needManager.deleteNeed(lNeed1);
            listRecipes = needManager.findRecipesByIngredient(lIngr1);
            assertEquals("Nekorektni pocet receptu.", 0, listRecipes.size());
	}

	/**
	 * Test of updateNeed method, of class NeedManager.
	 */
	@Test
	public void testUpdateNeed() {
		System.out.println("updateNeed");
            Recipe lRecipe1 = new Recipe(1, "Cukrovi", "01-01-2014", "Honza");
            Ingredient lIngr1 = new Ingredient(1, "mouka");
            Ingredient lIngr2 = new Ingredient(2, "voda");
            Need lNeed1 = new Need(1, 1, 1, "kg");
            Need lNeed2 = new Need(1, 2, 250, "g");
            
            // Test, zda je při přidávání receptu s hodnotou null vyhozena výjimka.
            try {
                needManager.addNeed(null);
                fail("Mela byt vyhozena vyjimka");

            }
            catch(IllegalArgumentException npe){
            }
            recipeManager.addRecipe(lRecipe1);
            ingrManager.addIngredient(lIngr1);
            needManager.addNeed(lNeed1);
            ingrManager.addIngredient(lIngr2);
            needManager.addNeed(lNeed2);
            needManager.updateNeed(lNeed2);
            List<Recipe> listRecipes = needManager.findRecipesByIngredient(lIngr1);
            assertEquals("Nekorektni pocet receptu.", 1, listRecipes.size());
            assertTrue("Chybi recept " + lRecipe1, listRecipes.contains(lRecipe1));
	}

	/**
	 * Test of findIngredientsByRecipe method, of class NeedManager.
	 */
	@Test
	public void testFindIngredientsByRecipe() {
		System.out.println("findIngredientsByRecipe");
			Recipe lRecipe1 = new Recipe(1, "Cukrovi", "01-01-2014", "Honza");
            Ingredient lIngr1 = new Ingredient(1, "mouka");
            Need lNeed1 = new Need(1, 1, 1, "kg");
            
            // Test, zda je při přidávání receptu s hodnotou null vyhozena výjimka.
            try {
                needManager.addNeed(null);
                fail("Mela byt vyhozena vyjimka");

            }
            catch(IllegalArgumentException npe){
            }
            recipeManager.addRecipe(lRecipe1);
            ingrManager.addIngredient(lIngr1);
            needManager.addNeed(lNeed1);
            List<Ingredient> listIngredients = needManager.findIngredientsByRecipe(lRecipe1);
            assertEquals("Nekorektni pocet receptu.", 1, listIngredients.size());
            assertTrue("Chybi surovina " + lRecipe1, listIngredients.contains(lIngr1));
	}

	/**
	 * Test of findRecipesByIngredient method, of class NeedManager.
	 */
	@Test
	public void testFindRecipesByIngredient() {
		System.out.println("findRecipesByIngredient");
		Recipe lRecipe1 = new Recipe(1, "Cukrovi", "01-01-2014", "Honza");
            Ingredient lIngr1 = new Ingredient(1, "mouka");
            Need lNeed1 = new Need(1, 1, 1, "kg");
            
            // Test, zda je při přidávání receptu s hodnotou null vyhozena výjimka.
            try {
                needManager.addNeed(null);
                fail("Mela byt vyhozena vyjimka");

            }
            catch(IllegalArgumentException npe){
            }
            recipeManager.addRecipe(lRecipe1);
            ingrManager.addIngredient(lIngr1);
            needManager.addNeed(lNeed1);
            List<Recipe> listRecipes = needManager.findRecipesByIngredient(lIngr1);
            assertEquals("Nekorektni pocet receptu.", 1, listRecipes.size());
            assertTrue("Chybi recept " + lRecipe1, listRecipes.contains(lRecipe1));
	}	
}
