/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import common.DBUtils;
import org.apache.commons.dbcp.BasicDataSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementace testů pro třídu RecipeManager.
 * @author Ondřej Staněk
 * @author Boris Valo
 */
public class RecipeManagerTest {
    
    private RecipeManagerImpl recipeManager;
    private DataSource ds;
    
    public RecipeManagerTest() {
    }
    
    public static DataSource prepareDataSource() throws SQLException {
        BasicDataSource ds = new BasicDataSource();
	ds.setUrl("jdbc:derby://localhost:1527/ROOT"); 
        ds.setUsername("root");
        ds.setPassword("root");
        return ds;
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws SQLException {
        ds = prepareDataSource();
        recipeManager = new RecipeManagerImpl();
        recipeManager.setDataSource(ds);
        DBUtils.executeSqlScript(ds, RecipeManager.class.getResource("createTables.sql"));
    }
    
    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(ds, RecipeManager.class.getResource("dropTables.sql"));
    }

    /**
     * Test of addRecipe method, of class RecipeManager.
     * @author Boris Valo
     */
    @Test
    public void testAddRecipe() {
        System.out.println("addRecipe");
        
        Recipe recipe1 = new Recipe(1, "Kuře na paprice", "01-01-2014", "Franta Vomáčka");
        Recipe recipe2 = new Recipe(2, "Houbová omáčka", "02-02-2014", "Pepa Novák");
        List<Recipe> recipes = recipeManager.getRecipes();
        
        // Test, zda je při přidávání receptu s hodnotou null vyhozena výjimka.
        try {
            recipeManager.addRecipe(null);
            fail("Mela byt vyhozena vyjimka");
            
        }
        catch(IllegalArgumentException npe){
        }
        
        // Test na prázdný seznam receptů.
        assertTrue("Seznam měl být prazdný.", recipes.isEmpty());
        
        // Vyvoření dvou receptů a jejich přidání do seznamu receptů.
        recipeManager.addRecipe(recipe1);
        recipeManager.addRecipe(recipe2);
        
        recipes = recipeManager.getRecipes();
       
        // Test na počet receptů v seznamu receptů.
        assertEquals("Nekorektni pocet receptu.", 2, recipes.size());
        
        // Test, zda jsou obsaženy očekávané recepty.
        assertTrue("Chybi recept " + recipe1, recipes.contains(recipe1));
        assertTrue("Chybi recept " + recipe2, recipes.contains(recipe2));
    }

    /**
     * Test of deleteRecipe method, of class RecipeManager.
     * @author Boris Valo
     */
    @Test
    public void testDeleteRecipe() {
        System.out.println("deleteRecipe");
        
        Recipe recipe1 = new Recipe(1, "Kuře na paprice", "01-01-2014", "Franta Vomáčka");
        List<Recipe> recipes = recipeManager.getRecipes();
        
        // Test na prázdný seznam receptů.
        assertTrue("Seznam měl být prázdný.", recipes.isEmpty());
        
        // Přidání jednoho receptu a test, zda byl přidán.
        recipeManager.addRecipe(recipe1);
        recipes = recipeManager.getRecipes();
        assertEquals("Nekorektni pocet receptu.", 1, recipes.size());
        assertTrue("V seznamu nebyl nalezen očekávaný recept: " + recipe1, recipes.contains(recipe1));
        
        
        // Vymazání receptu a test, zda byl vymazán.
        recipeManager.deleteRecipe(recipe1);
        recipes = recipeManager.getRecipes();
        assertTrue("Seznam měl být prázdný", recipes.isEmpty());
    }

    /**
     * Test of updateRecipe method, of class RecipeManager.
     * @author Ondřej Staněk
     */
    @Test
    public void testUpdateRecipe() {
        System.out.println("updateRecipe");
        
        Recipe recipe1 = new Recipe(1, "Kuře na paprice", "01-01-2014", "Franta Vomáčka");
        Recipe newRecipe = new Recipe(1, "Houbová omáčka", "02-02-2014", "Pepa Novák");
        
        // Do recipe1 dám recipe2.
        recipeManager.addRecipe(recipe1);
        recipeManager.updateRecipe(newRecipe);
		
        Recipe recipe = recipeManager.findById(recipe1.getId());
        
        // Test, zda je recipe obsahuje stejné hodnoty jako recipe2.
        assertEquals("V recipe má být nová hodnota "+newRecipe, newRecipe, recipe);
        assertEquals("V recipe má být nová hodnota.", newRecipe.getName(), recipe.getName());
        assertEquals("V recipe má být nová hodnota autora "+newRecipe.getAuthor(), newRecipe.getAuthor(), recipe.getAuthor());
    }

    /**
     * Test of getRecipes method, of class RecipeManager.
     * @author Boris Valo
     */
    @Test
    public void testGetRecipes() {
        System.out.println("getRecipes");
        
        // Na začátku je seznam receptů prázdný.
        List<Recipe> emptyList = recipeManager.getRecipes();
        assertTrue("Seznam ma byt prazdny", emptyList.isEmpty());
        
        // Vytvořím očekávaný seznam receptů.
        List<Recipe> expected = new ArrayList<>();
        
        Recipe recipe1 = new Recipe(1, "Kuře na paprice", "01-01-2014", "Franta Vomáčka");
        Recipe recipe2 = new Recipe(2, "Houbová omáčka", "02-02-2014", "Pepa Novák");
        Recipe recipe3 = new Recipe(3, "Polévka", "02-02-2014", "Pepa Novák");
        
        expected.add(recipe1);
        expected.add(recipe2);
        expected.add(recipe3);
        
        // Přidám vytvořené recepty i do seznamu receptů.
        for(Recipe recipe:expected){
            recipeManager.addRecipe(recipe);
        }
        
        // Pomocná proměnná pro porovnání výsledků.
        List<Recipe> result = recipeManager.getRecipes();
        
        // Kontroluji délky obou seznamů.
        assertEquals("Nesouhlasí délky", expected.size(), result.size());
        
        // Procházím seznam receptů a kontroluji, zda obsahuje všechny očekávané recepty.
        for(Recipe recipe:expected){
            assertTrue("Seznam neobsahuje "+recipe, result.contains(recipe));
        }
    }

    /**
     * Test of findByIngredient method, of class RecipeManager.
     * @author Ondřej Staněk
     */
	/*
    @Test
    public void testFindByIngredient() {
        System.out.println("findByingredient");
        
        List<Ingredient> ingredients = new ArrayList<>();
        Ingredient ingredient1 = new Ingredient(1, "Mouka");
        Ingredient ingredient2 = new Ingredient(2, "Voda");
        ingredients.add(ingredient1);
        
        Recipe recipe1 = new Recipe(1, "Kuře na paprice", "01-01-2014", "Franta Vomáčka");
        Recipe recipe2 = new Recipe(2, "Houbová omáčka", "02-02-2014", "Pepa Novák");
        
        List<Ingredient> ingredients2 = new ArrayList<>();
        ingredients2.add(ingredient2);
        Recipe recipe3 = new Recipe(3, "Polévka", "02-02-2014", "Pepa Novák");
        
        // Očekávám, že mi filtr vrátí dva recepty, které obsahují danou ingredienci.
        List<Recipe> expected = new ArrayList<>();
        expected.add(recipe1);
        expected.add(recipe2);
        
        // Přidám tři recepty, přičemž očekávanou ingredienci obsahují pouze dva z nich.
        recipeManager.addRecipe(recipe1);
        recipeManager.addRecipe(recipe2);
        recipeManager.addRecipe(recipe3);
        
        // Volám metodu a testuji, zda vrací správný seznam receptů.
        List<Recipe> result = recipeManager.findByIngredient(ingredient1);
        assertEquals(expected, result);
    }
	*/
	
    /**
     * Test of findById method, of class RecipeManager.
     * @author Ondřej Staněk
     */
    @Test
    public void testFindById() {
        System.out.println("findById");
        
        // Přidám do seznamu dva recepty.
        Recipe recipe1 = new Recipe(1, "Kuře na paprice", "01-01-2014", "Franta Vomáčka");
        Recipe expResult = new Recipe(2, "Houbová omáčka", "02-02-2014", "Pepa Novák");
        recipeManager.addRecipe(recipe1);
        recipeManager.addRecipe(expResult);
        
        // Hledám recept s id 2.
        int id = 2;
        Recipe result = recipeManager.findById(id);
        assertEquals("Id receptu se neshoduje s očekávaným id.", expResult, result);
    }

}
