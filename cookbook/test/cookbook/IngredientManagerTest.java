/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import common.DBUtils;
import static cookbook.RecipeManagerTest.prepareDataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author boris
 */
public class IngredientManagerTest {
	
	private IngredientManagerImpl ingrManager;
    private DataSource ds;
	
	public IngredientManagerTest() {
	}
	
	public static DataSource prepareDataSource() throws SQLException {
        BasicDataSource ds = new BasicDataSource();
        //ds.setUrl("jdbc:derby://localhost:1527/cookbookdb");
		ds.setUrl("jdbc:derby://localhost:1527/ROOT");
        ds.setUsername("root");
        ds.setPassword("root");
        return ds;
    }
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
    public void setUp() throws SQLException {
        ds = prepareDataSource();
        ingrManager = new IngredientManagerImpl();
        ingrManager.setDataSource(ds);
        DBUtils.executeSqlScript(ds, RecipeManager.class.getResource("createTables.sql"));
    }
    
    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(ds, RecipeManager.class.getResource("dropTables.sql"));
    }

	/**
	 * Test of addIngredient method, of class IngredientManager.
	 */
	@Test
	public void testAddIngredient() {
		System.out.println("addIngredient");
        
        Ingredient ingr1 = new Ingredient(1, "Kuře na paprice");
        Ingredient ingr2 = new Ingredient(2, "Houbová omáčka");
        List<Ingredient> ingredients = ingrManager.getIngredients();
        
        // Test, zda je při přidávání receptu s hodnotou null vyhozena výjimka.
        try {
            ingrManager.addIngredient(null);
            fail("Mela byt vyhozena vyjimka");
            
        }
        catch(IllegalArgumentException npe){
        }
        
        // Test na prázdný seznam receptů.
        assertTrue("Seznam měl být prazdný.", ingredients.isEmpty());
        
        // Vyvoření dvou receptů a jejich přidání do seznamu receptů.
        ingrManager.addIngredient(ingr1);
		ingrManager.addIngredient(ingr2);
        
        ingredients = ingrManager.getIngredients();
       
        // Test na počet receptů v seznamu receptů.
        assertEquals("Nekorektni pocet ingredienci.", 2, ingredients.size());
        
        // Test, zda jsou obsaženy očekávané recepty.
        assertTrue("Chybi ingredience " + ingr1, ingredients.contains(ingr1));
        assertTrue("Chybi ingredience " + ingr2, ingredients.contains(ingr2));
	}

	/**
	 * Test of deleteIngredient method, of class IngredientManager.
	 */
	@Test
	public void testDeleteIngredient() {
		System.out.println("deleteIngredient");
        
        Ingredient ingr = new Ingredient(1, "Kuře na paprice");
        List<Ingredient> ingredients = ingrManager.getIngredients();
        
        // Test na prázdný seznam receptů.
        assertTrue("Seznam měl být prázdný.", ingredients.isEmpty());
        
        // Přidání jednoho receptu a test, zda byl přidán.
        ingrManager.addIngredient(ingr);
        ingredients = ingrManager.getIngredients();
        assertEquals("Nekorektni pocet receptu.", 1, ingredients.size());
        assertTrue("V seznamu nebyl nalezen očekávaný recept: " + ingr, ingredients.contains(ingr));
        
        
        // Vymazání receptu a test, zda byl vymazán.
        ingrManager.deleteIngredient(ingr);
        ingredients = ingrManager.getIngredients();
        assertTrue("Seznam měl být prázdný", ingredients.isEmpty());
	}

	/**
	 * Test of updateIngredient method, of class IngredientManager.
	 */
	@Test
	public void testUpdateIngredient() {
		System.out.println("updateIngredient");
		
		Ingredient ingr = new Ingredient(1, "Maso");
		Ingredient newIngr = new Ingredient(1, "Kuřecí maso");
        
        // Do recipe1 dám recipe2.
        ingrManager.addIngredient(ingr);
        ingrManager.updateIngredient(newIngr);
		
        Ingredient ingrLoaded = ingrManager.findById(ingr.getId());
        
        // Test, zda je recipe obsahuje stejné hodnoty jako recipe2.
        assertEquals("V ingr má být nová hodnota "+newIngr, newIngr, ingrLoaded);
	}

	/**
	 * Test of getIngredients method, of class IngredientManager.
	 */
	@Test
	public void testGetIngredients() {
		System.out.println("getIngredients");
		
		// Na začátku je seznam receptů prázdný.
        List<Ingredient> emptyList = ingrManager.getIngredients();
        assertTrue("Seznam ma byt prazdny", emptyList.isEmpty());
        
        // Vytvořím očekávaný seznam receptů.
        List<Ingredient> expected = new ArrayList<>();
        
        Ingredient ingr1 = new Ingredient(1, "Kuře");
        Ingredient ingr2 = new Ingredient(2, "Houba");
        Ingredient ingr3 = new Ingredient(3, "Máslo");
        
        expected.add(ingr1);
        expected.add(ingr2);
        expected.add(ingr3);
        
        // Přidám vytvořené recepty i do seznamu receptů.
        for(Ingredient ingr:expected){
            ingrManager.addIngredient(ingr);
        }
        
        // Pomocná proměnná pro porovnání výsledků.
        List<Ingredient> result = ingrManager.getIngredients();
        
        // Kontroluji délky obou seznamů.
        assertEquals("Nesouhlasí délky", expected.size(), result.size());
        
        // Procházím seznam receptů a kontroluji, zda obsahuje všechny očekávané recepty.
        for(Ingredient ingr:expected){
            assertTrue("Seznam neobsahuje "+ingr, result.contains(ingr));
        }
	}

	/**
	 * Test of findById method, of class IngredientManager.
	 */
	@Test
	public void testFindById() {
		System.out.println("findById");
		
		// Přidám do seznamu dva recepty.
        Ingredient ingr = new Ingredient(1, "Kuře na paprice");
        Ingredient expResult = new Ingredient(2, "Houbová omáčka");
        ingrManager.addIngredient(ingr);
        ingrManager.addIngredient(expResult);
        
        // Hledám recept s id 2.
        int id = 2;
        Ingredient result = ingrManager.findById(id);
        assertEquals("Id ingredience se neshoduje s očekávaným id.", expResult, result);
	}
	
}
