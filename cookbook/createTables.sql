CREATE TABLE RECIPES (
    ID INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    NAME VARCHAR(100),
    CREATEDATE VARCHAR(10),
    CREATEPERSON VARCHAR(50)
);
CREATE TABLE INGREDIENT (
    ID INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    NAME VARCHAR(100)
);
CREATE TABLE NEED (
    INGREDIENTID INTEGER,
    RECIPEID INTEGER,
    AMOUNT FLOAT,
    UNIT VARCHAR(5)
);
