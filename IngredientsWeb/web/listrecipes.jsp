<%-- 
    Document   : listrecipes
    Created on : 14.4.2014, 10:01:03
    Author     : boris
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Seznam surovin</title>
    </head>
    <body>
        <h1>Seznam surovin</h1>
        <table border="1">
            <thead>
                <tr>
                    <th>Název</th>
                    <th/>
                    <th/>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${recipes}" var="recipe">
                    <tr>
                        <form action="${pageContext.request.contextPath}/UpdateRecipeServlet" method="post">
                        <input id="id" type="hidden" name="id" value="<c:out value="${recipe.id}"/>"/>
                        <td><input id="id" type="text" name="name" value="<c:out value="${recipe.name}"/>"/></td>
						<td><input id="id" type="text" name="author" value="<c:out value="${recipe.author}"/>"/></td>
						<td><c:out value="${recipe.createDate}"/></td>
                        <td><input type="submit" value="Upravit"/></td>
                        </form>
                        <form action="${pageContext.request.contextPath}/DeleteRecipeServlet" method="post">
                        <input id="id" type="hidden" name="id" value="<c:out value="${recipe.id}"/>"/>
                        <td><input type="submit" value="Vymazat"/></td>
                        </form>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <a href="${pageContext.request.contextPath}/addrecipe.jsp">Přidat recept</a>
    </body>
</html>
