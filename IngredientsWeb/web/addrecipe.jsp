<%-- 
    Document   : addrecipe
    Created on : 14.4.2014, 9:57:38
    Author     : boris
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Přidání jídla</title>
    </head>
    <body>
        <c:if test="$not empty error">
            <c:out value="${error}"/>
        </c:if>
        <h1>Přidání jídla</h1>
        <form action="${pageContext.request.contextPath}/RecipeServlet" method="post">
        <table>
            
            <tr>
                <td><label for="name">Název jídla:</label></td>
                <td><input type="text" id="name" name="name" size="30"/></td>
            </tr>
            <tr>
                <td><label for="author">Vaše jméno:</label></td>
                <td><input type="text" id="author" name="author" size="30"/></td>
            </tr>
        </table>
        <input type="submit" value="Přidat"/>
        </form>
    </body>
</html>

