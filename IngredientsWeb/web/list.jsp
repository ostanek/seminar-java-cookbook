<%-- 
    Document   : list
    Created on : 10.4.2014, 14:36:10
    Author     : Ondřej Staněk
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Seznam surovin</title>
    </head>
    <body>
        <h1>Seznam surovin</h1>
        <table border="1">
            <thead>
                <tr>
                    <th>Název</th>
                    <th/>
                    <th/>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${ingredients}" var="ingr">
                    <tr>
                        <form action="${pageContext.request.contextPath}/UpdateIngredientServlet" method="post">
                        <input id="id" type="hidden" name="id" value="<c:out value="${ingr.id}"/>"/>
                        <td><input id="id" type="text" name="name" value="<c:out value="${ingr.name}"/>"/></td>
                        <td><input type="submit" value="Upravit"/></td>
                        </form>
                        <form action="${pageContext.request.contextPath}/DeleteIngredientServlet" method="post">
                        <input id="id" type="hidden" name="id" value="<c:out value="${ingr.id}"/>"/>
                        <td><input type="submit" value="Vymazat"/></td>
                        </form>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <a href="${pageContext.request.contextPath}/add.jsp">Přidat surovinu</a>
    </body>
</html>
