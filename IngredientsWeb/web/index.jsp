<%-- 
    Document   : index
    Created on : 11.4.2014, 9:41:18
    Author     : Ondřej Staněk
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Suroviny</title>
    </head>
    <body>
        <h1>Suroviny</h1>
        <a href="${pageContext.request.contextPath}/add.jsp">Přidat surovinu</a> | 
		<a href="${pageContext.request.contextPath}/addrecipe.jsp">Přidat recept</a> <br />
        <form action="${pageContext.request.contextPath}/ShowIngredientsServlet" method="post">
        <input type="submit" value="Zobrazit všechny suroviny"/>
        </form>
		<form action="${pageContext.request.contextPath}/ShowRecipesServlet" method="post">
        <input type="submit" value="Zobrazit všechny recepty"/>
        </form>
    </body>
</html>
