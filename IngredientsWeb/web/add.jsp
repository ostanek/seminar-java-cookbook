<%-- 
    Document   : add
    Created on : 10.4.2014, 13:38:40
    Author     : Ondřej Staněk
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Suroviny</title>
    </head>
    <body>
        <c:if test="$not empty error">
            <c:out value="${error}"/>
        </c:if>
        <h1>Přidání suroviny</h1>
        <form action="${pageContext.request.contextPath}/IngredientServlet" method="post">
        <table>
            <tr>
                <td><label for="name">Název suroviny:</label></td>
                <td><input type="text" id="name" name="name" size="25"/></td>
            </tr>
        </table>
        <input type="submit" value="Přidat"/>
        </form>
        <form action="${pageContext.request.contextPath}/ShowIngredientsServlet" method="post">
        <input type="submit" value="Zobrazit všechny suroviny"/>
        </form>
    </body>
</html>
