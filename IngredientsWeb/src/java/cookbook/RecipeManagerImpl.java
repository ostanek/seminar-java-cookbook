/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cookbook;

import common.DBUtils;
import common.ServiceFailureException;
import common.IllegalEntityException;
import common.ValidationException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author Ondřej Staněk
 */
public class RecipeManagerImpl implements RecipeManager {
    
    private static final Logger logger = Logger.getLogger(
            RecipeManagerImpl.class.getName());

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }    

    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }

    @Override
    public void addRecipe(Recipe recipe) throws ServiceFailureException {
        
        checkDataSource();
        
        if (recipe == null)
            throw new IllegalArgumentException("Recipe is null.");
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("INSERT INTO RECIPES (NAME, CREATEDATE, CREATEPERSON) VALUES (?,?,?)");
            st.setString(1,recipe.getName());
            st.setString(2,recipe.getCreateDate());
            st.setString(3,recipe.getAuthor());
            st.execute();
            
            // Zjistim si ID receptu.
            /*st = null;
            st = conn.prepareStatement("SELECT * FROM RECIPES " +
                                       "WHERE NAME = ? AND CREATEDATE = ? AND CREATEPERSON = ?");
            st.setString(1,recipe.getName());
            st.setString(2,recipe.getCreateDate());
            st.setString(3,recipe.getAuthor());
            ResultSet returnedRecipes = st.executeQuery();
            int lIdRecipe = returnedRecipes.getInt("ID");
            
            // Pokud surovina jeste neni v tabulce ingredienci, tak ji tam pridam.
            for (Ingredient lIgredient:recipe.getIngredients()) {
                st = conn.prepareStatement("SELECT * FROM INGREDIENTS");
                ResultSet returnedIngredients = st.executeQuery();
            
                if(!returnedIngredients.next()) {
                    st = conn.prepareStatement("INSERT INTO INGREDIENT (NAME) VALUES (?)");
                    st.setString(1, lIgredient.getName());
                    st.execute();
                }
            }
            
            // Priradim suroviny k receptu pridanim radku do tabulky NEED.
            for (Ingredient lIgredient:recipe.getIngredients()) {
                
                // Zjistim si ID suroviny.
                st = conn.prepareStatement("SELECT ID FROM INGREDIENTS WHERE NAME = ?");
                st.setString(1, lIgredient.getName());
                ResultSet returnedIngredients = st.executeQuery();
                int lIdIngredient = returnedIngredients.getInt("ID");
                
                st = conn.prepareStatement("INSERT INTO INGREDIENT (ID, NAME) VALUES (?,?,?)");
                st.setInt(1, lIdRecipe);
                st.setInt(2, lIdIngredient);
                st.execute();
            }*/
            
        } catch (SQLException ex) {
            String msg = "Error when trying to add recipe " + recipe;
            Logger.getLogger(RecipeManagerImpl.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
    }

    @Override
    public void deleteRecipe(Recipe recipe) {
        
        checkDataSource();
        
        if (recipe == null)
                throw new NullPointerException();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("DELETE FROM RECIPES WHERE ID = ?");
            st.setInt(1, recipe.getId());
            st.execute();
            
        } catch (SQLException ex) {
            String msg = "Error when trying to delete recipe " + recipe;
            Logger.getLogger(RecipeManagerImpl.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
    }

    @Override
    public void updateRecipe(Recipe newRecipe) {
        
        checkDataSource();
        
        if (newRecipe == null)
            throw new NullPointerException();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("UPDATE RECIPES SET NAME = ?, CREATEPERSON = ? WHERE ID = ?");
            st.setString(1, newRecipe.getName());
            //st.setString(2, newRecipe.getCreateDate());
            st.setString(2, newRecipe.getAuthor());
            st.setInt(3, newRecipe.getId());
            st.execute();
            
        } catch (SQLException ex) {
            String msg = "Error when trying to update recipe " + newRecipe;
            Logger.getLogger(RecipeManagerImpl.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
    }

    @Override
    public List<Recipe> getRecipes() {
        
        checkDataSource();
        
        List<Recipe> recipes = new ArrayList<>();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT * FROM RECIPES");
            ResultSet returnedRecipes = st.executeQuery();
            
            while(returnedRecipes.next()) {
                Recipe recipe = new Recipe();
                recipe.setId(returnedRecipes.getInt("ID"));
                recipe.setName(returnedRecipes.getString("NAME"));
                recipe.setCreateDate(returnedRecipes.getString("CREATEDATE"));
                recipe.setAuthor(returnedRecipes.getString("CREATEPERSON"));
                recipes.add(recipe);
            }
            
        } catch (SQLException ex) {
            String msg = "Error when trying to get all recipes.";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
        
        return recipes;
    }

	/*
    @Override
    public List<Recipe> findByIngredient(Ingredient aIngredient) {
        
        checkDataSource();
        
        Connection conn = null;
        PreparedStatement st = null;
        
        Recipe recipe = null;
        List<Recipe> lRecipes = new ArrayList<>();
        List<Ingredient> lIngredients;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT RECIPES.ID FROM RECIPES JOIN NEED " +
                    "ON NEED.INGREDIENTID = RECIPES.ID " +
                    "WHERE RECIPES.ID = ?");
            st.setInt(1, aIngredient.getId());
            ResultSet returnedRecipes = st.executeQuery();
            
            while(returnedRecipes.next()) {
                recipe = new Recipe();
                recipe.setId(returnedRecipes.getInt("ID"));
                recipe.setName(returnedRecipes.getString("NAME"));
                recipe.setCreateDate(returnedRecipes.getString("CREATEDATE"));
                recipe.setAuthor(returnedRecipes.getString("CREATEPERSON"));
                lRecipes.add(recipe);
            }
            
            for (Recipe lRecipe:lRecipes) {
                lIngredients = lRecipe.getIngredients();
                for (Ingredient lIngredient:lIngredients) {
                    if (lIngredient.getId() == aIngredient.getId()) {
                        lRecipes.add(lRecipe);
                        break;
                    }
                }
            }
        } catch (SQLException ex) {
            String msg = "Error when trying to find recipe with ingredient named " + aIngredient.getName();
            Logger.getLogger(RecipeManagerImpl.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
        
        return lRecipes;
		
    }
	*/
    @Override
    public Recipe findById(int aId) {
        
        checkDataSource();
        
        Recipe recipe = null;
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT * FROM RECIPES WHERE ID = ?");
            st.setInt(1, aId);
            ResultSet returnedRecipes = st.executeQuery();
            
            while(returnedRecipes.next()) {
                recipe = new Recipe();
                recipe.setId(returnedRecipes.getInt("ID"));
                recipe.setName(returnedRecipes.getString("NAME"));
                recipe.setCreateDate(returnedRecipes.getString("CREATEDATE"));
                recipe.setAuthor(returnedRecipes.getString("CREATEPERSON"));
            }
            
        } catch (SQLException ex) {
            String msg = "Error when trying to find recipe with id " + aId;
            Logger.getLogger(RecipeManagerImpl.class.getName()).log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }
        finally {
            if (conn != null) {
                DBUtils.closeQuietly(conn, st);
            }
        }
        
        return recipe;
    }
    
}
