/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package webapp;

import common.DBUtils;
import cookbook.IngredientManager;
import cookbook.IngredientManagerImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.activity.InvalidActivityException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cookbook.Ingredient;
import java.util.List;
import javax.sql.DataSource;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;

/**
 *
 * @author Ondřej Staněk
 */
@WebServlet(name="IngredientServlet", urlPatterns = {"/IngredientServlet"})
public class IngredientServlet extends HttpServlet {
    
    private IngredientManagerImpl ingrManager;
    private DataSource ds;
    
    public static DataSource prepareDataSource() throws SQLException {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("org.apache.derby.jdbc.ClientDriver");
        ds.setUrl("jdbc:derby://localhost:1527/ROOT");
        ds.setUsername("root");
        ds.setPassword("root");
        System.out.println("Spojeni vytvoreno");
        return ds;
    }
    
    @Override
    public void init() {
        try {
            ds = prepareDataSource();
            ingrManager = new IngredientManagerImpl();
            ingrManager.setDataSource(ds);
            DBUtils.executeSqlScript(ds, IngredientManager.class.getResource("createTables.sql"));
            System.out.println("Vytvoreny tabulky");
        }
        catch (SQLException ex) {
            System.err.println("Chyba pri praci s databazi: "+ex.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet IngredientServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet IngredientServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        throw new InvalidActivityException("Post method is supported only!");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            String ingredientName=request.getParameter("name");
            if(!checkParams(ingredientName))
            {
                request.setAttribute("error", "Nutno zadat všechna pole!");
                request.getRequestDispatcher("/add.jsp").forward(request, response);
                return;
            }
            
            Ingredient ingredient = new Ingredient();
            ingredient.setName(ingredientName);
            ingrManager.addIngredient(ingredient);
            List<Ingredient> list = ingrManager.getIngredients();
            request.setAttribute("ingredients", list);
            request.getRequestDispatcher("/list.jsp").forward(request,response);
    }
    
    private boolean checkParams(String name) {
        return name != null;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
