/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package webapp;

import common.DBUtils;
import cookbook.Recipe;
import cookbook.RecipeManager;
import cookbook.RecipeManagerImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activity.InvalidActivityException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;

/**
 *
 * @author boris
 */
@WebServlet(name = "UpdateRecipeServlet", urlPatterns = {"/UpdateRecipeServlet"})
public class UpdateRecipeServlet extends HttpServlet {

	private RecipeManagerImpl recipeManager;
    private DataSource ds;
    
    public static DataSource prepareDataSource() throws SQLException {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("org.apache.derby.jdbc.ClientDriver");
        ds.setUrl("jdbc:derby://localhost:1527/ROOT");
        ds.setUsername("root");
        ds.setPassword("root");
        System.out.println("Spojeni vytvoreno");
        return ds;
    }
	
	@Override
	public void init(){
		try {
            ds = prepareDataSource();
            recipeManager = new RecipeManagerImpl();
            recipeManager.setDataSource(ds);
            DBUtils.executeSqlScript(ds, RecipeManager.class.getResource("createTables.sql"));
            System.out.println("Vytvoreny tabulky");
        }
        catch (SQLException ex) {
            System.err.println("Chyba pri praci s databazi: "+ex.getMessage());
        }
	}
	
	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			/* TODO output your page here. You may use following sample code. */
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Servlet UpdateRecipeServlet</title>");			
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Servlet UpdateRecipeServlet at " + request.getContextPath() + "</h1>");
			out.println("</body>");
			out.println("</html>");
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		throw new InvalidActivityException("Post method is supported only!");
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			String recipeId=request.getParameter("id");
            String name = request.getParameter("name");
			String author = request.getParameter("author");
            if(!checkParams(recipeId, name, author))
            {
                request.setAttribute("error", "Nutno zadat všechna pole!");
                List<Recipe> list = recipeManager.getRecipes();
                request.setAttribute("recipes", list);
                request.getRequestDispatcher("/listrecipes.jsp").forward(request,response);
                return;
            }
            
            int id;
            try{
                id = Integer.parseInt(recipeId);
            }catch(NumberFormatException nfe){
                Logger.getAnonymousLogger(DeleteIngredientServlet.class.getName()).log(Level.SEVERE,"Price is not an integer",nfe);
                request.setAttribute("error","Identifikator musi byt cele cislo!");
                List<Recipe> list = recipeManager.getRecipes();
                request.setAttribute("recipes", list);
                request.getRequestDispatcher("/listrecipes.jsp").forward(request,response);
                return;
            }
            
            Recipe recipe = new Recipe();
            recipe.setId(id);
            recipe.setName(name);
			recipe.setAuthor(author);
            recipeManager.updateRecipe(recipe);
            List<Recipe> list = recipeManager.getRecipes();
			request.setAttribute("recipes", list);
			request.getRequestDispatcher("/listrecipes.jsp").forward(request,response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	private boolean checkParams(String id, String name, String author) {
        return id != null && name != null && author != null;
    }
}
