/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package webapp;

import common.DBUtils;
import cookbook.Recipe;
import cookbook.RecipeManager;
import cookbook.RecipeManagerImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.activity.InvalidActivityException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import static webapp.ShowIngredientsServlet.prepareDataSource;

/**
 *
 * @author boris
 */
@WebServlet(name = "ShowRecipesServlet", urlPatterns = {"/ShowRecipesServlet"})
public class ShowRecipesServlet extends HttpServlet {

	private RecipeManagerImpl recipeManager;
    private DataSource ds;
	
	public static DataSource prepareDataSource() throws SQLException {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("org.apache.derby.jdbc.ClientDriver");
        ds.setUrl("jdbc:derby://localhost:1527/ROOT");
        ds.setUsername("root");
        ds.setPassword("root");
        System.out.println("Spojeni vytvoreno");
        return ds;
    }
	
	@Override
    public void init() {
        try {
            ds = prepareDataSource();
            recipeManager = new RecipeManagerImpl();
            recipeManager.setDataSource(ds);
            DBUtils.executeSqlScript(ds, RecipeManager.class.getResource("createTables.sql"));
            System.out.println("Vytvoreny tabulky");
        }
        catch (SQLException ex) {
            System.err.println("Chyba pri praci s databazi: "+ex.getMessage());
        }
    }
	
	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			/* TODO output your page here. You may use following sample code. */
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Servlet ShowRecipesServlet</title>");			
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Servlet ShowRecipesServlet at " + request.getContextPath() + "</h1>");
			out.println("</body>");
			out.println("</html>");
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		throw new InvalidActivityException("Post method is supported only!");
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		List<Recipe> list = recipeManager.getRecipes();
        request.setAttribute("recipes", list);
        request.getRequestDispatcher("/listrecipes.jsp").forward(request,response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}
